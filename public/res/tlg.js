// Define a bunch of elements:
let strokes     = document.querySelectorAll(".strokes"),
    darkness    = document.querySelector("#darkness"),
    countdown   = document.querySelector("countdown-timer"),
    cd          = $(countdown).clone(),
    credits1    = document.querySelectorAll(".credits1"),
    credits2    = document.querySelectorAll(".credits2"),
    credits3    = document.querySelectorAll(".credits3"),
    interval    = document.querySelector("#interval"),
    minValue    = 90,
    maxValue    = 120;
let diff        = maxValue - minValue;

// Listen for a keypress to stop the timer
document.addEventListener("keypress",function(e){
    clearInterval(intResource);
});

// This function sets off the entire show:
function countDown(){
    // We want to have a countdown timer when the page loads before we start playing stuff
    // This allows time for an OBS recording of the page to start and assets to load...
    
    // Let's get the number of seconds as defined by the text in the countdown element
    let seconds = countdown.innerText;

    // Cast seconds into a number, subtract one and see if it is less than or equal to zero:
    if(Number(seconds) - 1 <= 0){
        // Remove the countdown element and start the CSS animations:
        countdown.parentNode.removeChild(countdown);
        playCredits();
        playShirts();
    }
    else{
        // Otherwise we will set the innertext to the new number of seconds
        // And re-run this function after one second has elapsed...
        countdown.innerHTML = seconds - 1;
        setTimeout(function(){
            countDown();
        },1000);
    }
}

function playShirts(){
    // Function to trigger the animations for each shirt slightly offset from each other
    let shirts = document.querySelectorAll(".shirt");
    shirts[0].style.animationPlayState = "running";
    setTimeout(function(){
        shirts[1].style.animationPlayState = "running";
    },100);
    setTimeout(function(){
        shirts[2].style.animationPlayState = "running";
    },200)
}

let intResource = setInterval(function(){
    // Runs once every second for one minute
    // We grab the text of interval, convert it to a number, and set the interval's text to number
    let counter = interval.innerText;
    interval.innerText = Number(counter) + 1;

    // Check if we should clear this interval:
    if(Number(counter) >= 59){
        clearInterval(intResource)
    }
},1000);

function playCredits(){
    // Add the 'playing' class to the shirts.
    let target = $(".credits");
    target.find(".credits-container").addClass("playing");
}

// Finally we kick off the show:
countDown();


